from django.apps import AppConfig


class SnowTrailsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'snow_trails_rest'
