from django.db import models

# Create your models here.
DIFFICULTY_RATING = (
    ("green", "GREEN"),
    ("blue", "BLUE"),
    ("black_diamond", "BLACK_DIAMOND"),
    ("double_black_diamond", "DOUBLE_BLACK DIAMOND"),
    ("expert", "EXPERT"),
)


class Account(models.Model):
    name = models.CharField(max_length=200)
    email = models.CharField(unique=True)
    password = models.CharField(max_length=200)


class Trails(models.Model):
    account = models.ForeignKey(
        Account, related_name="trails", on_delete=models.CASCADE
    )
    description = models.CharField(max_length=200)
    picture_url = models.URLField()
    title = models.CharField(max_length=50)
    rating = models.PositiveSmallIntegerField()
    recommended_gear = models.CharField(max_length=200)
    hike_up_duration = models.PositiveSmallIntegerField()
    ride_down_duration = models.PositiveSmallIntegerField()
    difficulty = models.CharField(
        max_length=20, choices=DIFFICULTY_RATING, default="unrated"
    )
    location = models.CharField(max_length=200)


class Location(models.Models):
    city
    State
    coordinates
